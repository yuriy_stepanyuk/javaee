package com.stepanyuk.controllers;


import com.stepanyuk.db.Database;
import com.stepanyuk.enums.SearchType;
import com.stepanyuk.beans.Book;
import com.sun.javafx.collections.MappingChange;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean(eager = true, name = "bookListController")
@SessionScoped
public class BookListController {

    private SearchType searchType;
    private String searchString;
    private Map<String,SearchType> searchTypeMap = new HashMap<>();
    private List<Book> currentBookList = new ArrayList<>();

    private String currentSql;// последний выполнный sql без добавления limit
    private int booksOnPage = 2;
    private long selectedPageNumber = 1; // выбранный номер страницы в постраничной навигации
    private ArrayList<Integer> pageNumbers = new ArrayList<>(); // общее кол-во страниц
    private long totalBooksCount; // общее кол-во книг (не на текущей странице, а всего), нажно для постраничности
    private char selectedLetter; // выбранная буква алфавита
    private long selectedGenreId; // выбранный жанр

    private String userRole;

    private boolean editMode;

    public BookListController() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("nls.messagess", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        searchTypeMap.put(resourceBundle.getString("author_name"), SearchType.AUTHOR);
        searchTypeMap.put(resourceBundle.getString("book_name"), SearchType.TITLE);
//
//        HttpServletRequest httpServletRequest = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
//        httpServletRequest.isUserInRole("admin")

        fillBooksAll();
    }

    private void fillBooksAll() {
        fillBooksbySQL(getStringQueryAllBooks());
    }

    private void fillBooksbySQL(String queryStr) {

        currentBookList.clear();

        StringBuilder sqlBuilder = new StringBuilder(queryStr);
        currentSql = queryStr;

        if (queryStr == null) {
            queryStr =  "SELECT * FROM book";
        }

        ResultSet rs = null;

        try(Connection connection = Database.getConnection();
            Statement statement = connection.createStatement();
        ){

            rs = statement.executeQuery(sqlBuilder.toString());
            rs.last();
            totalBooksCount = rs.getRow();

            fillPageNumbers(totalBooksCount, booksOnPage);

            if (totalBooksCount > booksOnPage) {
                sqlBuilder.append(" limit ").append((selectedPageNumber-1) * booksOnPage).append(",").append(booksOnPage);
            }

//            rs = statement.executeQuery(sqlBuilder.toString());

            ResultSet resultSet = statement.executeQuery(sqlBuilder.toString());
            while (resultSet.next()){
                Book book = new Book();
                book.setId(resultSet.getLong("id"));
                book.setName(resultSet.getString("name"));
                book.setAuthor_id(resultSet.getLong("author_id"));
                book.setGenre_id(resultSet.getLong("genre_id"));
                book.setIsbn(resultSet.getString("isbn"));
                book.setPage_count(resultSet.getInt("page_count"));
                book.setPublish_year(resultSet.getDate("publish_year"));
                book.setPublisher_id(resultSet.getLong("publisher_id"));
                book.setPublisher(resultSet.getString("publisher"));
                book.setAutor(resultSet.getString("author"));
                book.setGenre(resultSet.getString("genre"));
                book.setImage(resultSet.getBytes("image"));
                book.setDescr(resultSet.getString("descr"));

                currentBookList.add(book);
            }
        } catch (SQLException e) {
            Logger.getLogger(BookListController.class.getName()).log(Level.SEVERE,null,e);
        } finally {
            try {
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(BookListController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private String getStringQueryAllBooks(){

        return "SELECT " +
                "    books.image, " +
//                "    books.content, " +
                "    books.id, " +
                "    books.isbn, " +
                "    books.name, " +
                "    books.page_count, " +
                "    books.genre_id, " +
                "    books.author_id, " +
                "    books.publisher_id, " +
                "    books.publish_year, " +
                "    genretab.name  as genre," +
                "    autortab.fio  as author," +
                "    publishertab.name  as publisher, " +
                "    books.descr " +
                "FROM library.book as books " +
                "left join library.author as autortab on autortab.id = books.author_id " +
                "left join library.genre as genretab on genretab.id = books.genre_id " +
                "left join library.publisher as publishertab on publishertab.id = books.publisher_id ";
    }

    public void fillBooksByGenre() {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        Long id = Long.valueOf(params.get("genre_id"));

        selectedPageNumber  = 1;
        selectedGenreId     = id;
        selectedLetter = ' ';

        if (id == Long.MAX_VALUE) {
            fillBooksAll();
        } else {

            fillBooksbySQL("SELECT " +
                    "    books.image, " +
//                    "    books.content, " +
                    "    books.id, " +
                    "    books.isbn, " +
                    "    books.name, " +
                    "    books.page_count, " +
                    "    books.genre_id, " +
                    "    books.author_id, " +
                    "    books.publisher_id, " +
                    "    books.publish_year, " +
                    "    genretab.name  as genre," +
                    "    autortab.fio  as author," +
                    "    publishertab.name  as publisher, " +
                    "    books.descr " +
                    "FROM library.book as books " +
                    "left join library.author as autortab on autortab.id = books.author_id " +
                    "left join library.genre as genretab on genretab.id = books.genre_id " +
                    "left join library.publisher as publishertab on publishertab.id = books.publisher_id " +
                    "where books.genre_id = " + id + " order by books.name ");

        }
    }

    public void fillBooksByLetter(){

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String letter = params.get("letter_search");

        selectedLetter = letter.charAt(0);
        selectedPageNumber = 1;

        fillBooksbySQL("SELECT " +
                "    books.image, " +
                "    books.id, " +
                "    books.isbn, " +
                "    books.name, " +
                "    books.page_count, " +
                "    books.genre_id, " +
                "    books.author_id, " +
                "    books.publisher_id, " +
                "    books.publish_year, " +
                "    genretab.name  as genre," +
                "    autortab.fio  as author," +
                "    publishertab.name  as publisher, " +
                "    books.descr " +
                "FROM library.book as books " +
                "left join library.author as autortab on autortab.id = books.author_id " +
                "left join library.genre as genretab on genretab.id = books.genre_id " +
                "left join library.publisher as publishertab on publishertab.id = books.publisher_id " +
                "where  substr(books.name, 1,1) = '" + letter + "' order by books.name ");

        selectedGenreId = -1;
        selectedPageNumber = 1;

    }

    public void fillBooksBySearchString(){

//        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//        String stringSearch = params.get("search_string");

//        SearchType type = SearchType.valueOf(searchType);

        StringBuilder sqlStr = new StringBuilder(
                "SELECT " +
                        "    books.image, " +
//                        "    books.content, " +
                        "    books.id, " +
                        "    books.isbn, " +
                        "    books.name, " +
                        "    books.page_count, " +
                        "    books.genre_id, " +
                        "    books.author_id, " +
                        "    books.publisher_id, " +
                        "    books.publish_year, " +
                        "    genretab.name  as genre," +
                        "    autortab.fio  as author," +
                        "    publishertab.name  as publisher, " +
                        "    books.descr " +
                        "FROM library.book as books " +
                        "left join library.author as autortab on autortab.id = books.author_id " +
                        "left join library.genre as genretab on genretab.id = books.genre_id " +
                        "left join library.publisher as publishertab on publishertab.id = books.publisher_id ");

        if (searchType == SearchType.TITLE) {
            sqlStr.append(" where lower(books.name) like '%"+ searchString.toLowerCase().trim() + "%' order by books.name");
        } else if (searchType == SearchType.AUTHOR){
            sqlStr.append(" where lower(autortab.fio) like '%"+ searchString.toLowerCase().trim() +"%' order by books.name");
        }

//        sqlStr.append(" limit 0,5 ");

        fillBooksbySQL(sqlStr.toString());

        selectedLetter = ' ';
        selectedGenreId = -1;
        selectedPageNumber = 1;

    }

    private void fillPageNumbers(long totalBooksCount, int booksCountOnPage) {

        int pageCount = totalBooksCount > 0 ? (int) (totalBooksCount / booksCountOnPage) : 0;

        if (pageCount  - ((double)totalBooksCount / booksCountOnPage) < 0) {
            pageCount += 1;
        }

        pageNumbers.clear();
        for (int i = 1; i <= pageCount; i++) {
            pageNumbers.add(i);
        }

    }

    public byte[] getImage(long id) {

        byte[] image = null;

        try(Connection connection = Database.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select image from book where id=" + id)) {

            while (resultSet.next()) {
                image = resultSet.getBytes("image");
            }

        } catch (SQLException ex) {
            Logger.getLogger(Book.class.getName()).log(Level.SEVERE, null, ex);
        }

        return image;
    }

    public byte[] getContent(int id) {

        byte[] content = null;

        try(Connection connection = Database.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select content from book where id=" + id)) {

            while (resultSet.next()) {
                content = resultSet.getBytes("content");
            }

        } catch (SQLException ex) {
            Logger.getLogger(Book.class.getName()).log(Level.SEVERE, null, ex);
        }

        return content;

    }

    public List<Character> getLetterListChar(){

        char[] charList = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

        List<Character> result = new ArrayList<>();

        for (char curChar:charList
                ) {

            result.add(curChar);

        }

        return result;
    }

    public String selectPage() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        selectedPageNumber = Integer.valueOf(params.get("page_number"));
        fillBooksbySQL(currentSql);
        return "books";
    }

    public String updateBooks(){

        try(Connection connection = Database.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE book SET isbn=?, name=?, page_count=?, publish_year=?, descr=?  WHERE id=?");
        ){

            for (Book curBook:currentBookList
                 ) {
                if (!curBook.isEdit()) continue;
                preparedStatement.setString(1, curBook.getIsbn());
                preparedStatement.setString(2, curBook.getName());
                preparedStatement.setInt(3,curBook.getPage_count());
                preparedStatement.setDate(4, (Date) curBook.getPublish_year());
                preparedStatement.setString(5, curBook.getDescr());
                preparedStatement.setLong(6, curBook.getId());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();

        } catch (SQLException e) {
            Logger.getLogger(BookListController.class.getName()).log(Level.SEVERE,null,e);
        }

        switchEditMode();
        cancelEditMode();
        return "books";
    }

    public boolean acces(){

        HttpServletRequest httpServletRequest = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
        int r =2;

        return true;
    }

    public boolean isEditMode(){
        return this.editMode;
    }

    public void switchEditMode(){
        this.editMode = !this.editMode;
    }

    public void cancelEditMode(){
        this.editMode = false;
        for (Book curBook: currentBookList
             ) {
            curBook.setEdit(false);
        }
    }

    public void searchTypeChanged(ValueChangeEvent e) {
        searchType = (SearchType) e.getNewValue();
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType){
        this.searchType = searchType;
    }

    public Map<String, SearchType> getSearchTypeMap() {
        return searchTypeMap;
    }

    public List<Book> getCurrentBookList() {
        return currentBookList;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public void setSearchTypeMap(Map<String, SearchType> searchTypeMap) {
        this.searchTypeMap = searchTypeMap;
    }

    public void setCurrentBookList(List<Book> currentBookList) {
        this.currentBookList = currentBookList;
    }

    public int getBooksOnPage() {
        return booksOnPage;
    }

    public void setBooksOnPage(int booksOnPage) {
        this.booksOnPage = booksOnPage;
    }

    public long getSelectedPageNumber() {
        return selectedPageNumber;
    }

    public void setSelectedPageNumber(long selectedPageNumber) {
        this.selectedPageNumber = selectedPageNumber;
    }

    public String getCurrentSql() {
        return currentSql;
    }

    public void setCurrentSql(String currentSql) {
        this.currentSql = currentSql;
    }

    public ArrayList<Integer> getPageNumbers() {
        return pageNumbers;
    }

    public void setPageNumbers(ArrayList<Integer> pageNumbers) {
        this.pageNumbers = pageNumbers;
    }

    public long getTotalBooksCount() {
        return totalBooksCount;
    }

    public void setTotalBooksCount(long totalBooksCount) {
        this.totalBooksCount = totalBooksCount;
    }

    public char getSelectedLetter() {
        return selectedLetter;
    }

    public void setSelectedLetter(char selectedLetter) {
        this.selectedLetter = selectedLetter;
    }

    public long getSelectedGenreId() {
        return selectedGenreId;
    }

    public void setSelectedGenreId(long selectedGenreId) {
        this.selectedGenreId = selectedGenreId;
    }

    public void changeBooksCountOnPage(ValueChangeEvent e) {
//        imitateLoading();
        cancelEditMode();
//        pageSelected = false;
        booksOnPage = Integer.valueOf(e.getNewValue().toString()).intValue();
        selectedPageNumber = 1;
        fillBooksbySQL(getStringQueryAllBooks());
    }

//    public Map<String, String> getSelectedPages(){
//        Map<String, String> result = new HashMap<>();
//        result.put("2", "2");
//        result.put("5", "5");
//        result.put("10", "10");
//
//        return result;
//    }

    private void imitateLoading() {
        try {
            Thread.sleep(1000);// имитация загрузки процесса
        } catch (InterruptedException ex) {
            Logger.getLogger(BookListController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
