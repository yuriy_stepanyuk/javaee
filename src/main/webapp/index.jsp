<html>

<head>
    <meta http-equiv="content-type" content="text/css; charset=UTF-8">
    <link href="./css/style_index.css" type="text/css" rel="stylesheet">
</head>

<body>

<div class="pagestyle">

    <div class="header">
        <p class="log_image"><img src="./images/lib.png" width="76" height="77"></p>
        <h2>Welcome to book Library</h2>
    </div>

    <div class="dataform">

        <form action="pages/main.jsp" method="post" name="userinput">
            <label for="id_username">User name: </label>
            <input type="text" name="username" id="id_username" placeholder="username">

            <div class="buttonsgroup">
                <input type="submit" name="LogIn" value="Log In" class="buttons">
            </div>

        </form>

    </div>

    <div class="footer">
        Developer: Stepanyuk Y. V. 2016
    </div>

</div>

</body>
</html>
