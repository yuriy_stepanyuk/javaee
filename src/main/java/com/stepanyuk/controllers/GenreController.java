package com.stepanyuk.controllers;


import com.stepanyuk.beans.Genre;
import com.stepanyuk.db.Database;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean(name = "genreController")
@ApplicationScoped
public class GenreController implements Serializable {

    private List<Genre> genreList = new ArrayList<>();

    public GenreController() {
        genreList = getGenreList();
    }

    public List<Genre> getGenreList() {
        if (!genreList.isEmpty()) {
            return genreList;
        } else {
            return getGenres();
        }

    }

    private List<Genre> getGenres() {

        try(Connection connection= Database.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM genre")){

            ResourceBundle resourceBundle = ResourceBundle.getBundle("nls.messagess", FacesContext.getCurrentInstance().getViewRoot().getLocale());

            Genre genreAll = new Genre();
            genreAll.setId(Long.MAX_VALUE);
            genreAll.setName(resourceBundle.getString("book_all"));
            genreList.add(genreAll);

            while (resultSet.next()){
                Genre genre = new Genre();
                genre.setId(resultSet.getLong("id"));
                genre.setName(resultSet.getString("name"));

                genreList.add(genre);
            }

        } catch (SQLException e) {
            Logger.getLogger(GenreController.class.getName()).log(Level.SEVERE,null,e);
        }

        return genreList;
    }
}
